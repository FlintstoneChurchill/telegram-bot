const {Telegraf} = require("telegraf");
const _ = require("lodash");

const token = '1455663140:AAHOe_LX5ZkJxvS5RsXNbCUcg9rcxacKdYk';

const bot = new Telegraf(token);

const stringifyQuestion = question => {
  return `
    ${question.category}: ${question.difficulty}
    ${question.question}
    ------------------------
    Variants:
    ${question.possibleAnswers.join(" | ")}
  `;
};

bot.start(ctx => {
  ctx.reply("Привет! Для того, чтобы начать игру введите команду /start_game");
});
bot.help(async ctx => {
  const commands = await bot.telegram.getMyCommands();
  let message = "";
  commands.forEach(command => {
    message += `/${command.command} - ${command.description}\n`;
  });
  ctx.reply(message);
});


bot.launch()